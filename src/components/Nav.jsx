import React from "react";

export default function Nav() {
  return (
    <div className="shadow">
      <div className="container">
        <nav className="d-flex justify-content-between py-3 align-items-center ">
          <div className="nav-link">Navbar</div>
          <div className="btn btn-primary">Sign In</div>
        </nav>
      </div>
    </div>
  );
}
