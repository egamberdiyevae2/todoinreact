import React from "react";

export default function Card({ name, role, src }) {
  return (
    <div className="card mt-5">
      <img className="card-img-top w-100" src={src} alt="img" />
      <div className="card-img-overlay text-white">
        <h4 className="card-title">{name}</h4>
        <p className="card-text">{role}</p>
        <a href="#" className="btn btn-primary">
          See Profile
        </a>
      </div>
    </div>
  );
}
