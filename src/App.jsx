// import React, { useState } from "react";
// import Nav from "./components/Nav";
// // import Card from "./components/card";

// export default function App() {
//   const [name, setName] = useState("akbar");

//   // const add = () => {
//   //   // const array = [];
//   //   // console.log(name);
//   //   // array.push(name);
//   //   // console.log(array);
//   //   // setName = "";

//   // };

//   return (
//     <div>
//       <Nav />
//       <div className="container">
//         <div className="row">
//           <form action="#" className="form mt-5">
//             <input
//               value={name}
//               onChange={(e) => setName(e.target.value)}
//               type="text"
//               className="form-control my-3"
//             />
//             <button className="btn btn-info " onClick={() => setName("")}>
//               Clear
//             </button>
//             {/* <button className="btn btn-success " onClick={}>
//               add
//             </button> */}
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }

// !!
// !!
// !!
// !!
// !!
// !!
// !!
// !!
// !!

import React, { useState } from "react";
import Nav from "./components/Nav";

export default function App() {
  const [name, setName] = useState("");
  const [todos, setTodos] = useState([]);
  const [editingIndex, setEditingIndex] = useState(-1);
  const [editText, setEditText] = useState("");

  const addTodo = () => {
    if (name.trim() !== "") {
      setTodos([...todos, name]);
      setName("");
    }
  };

  const editTodo = (index) => {
    setEditingIndex(index);
    setEditText(todos[index]);
  };

  const updateTodo = () => {
    if (editText.trim() !== "") {
      const updatedTodos = [...todos];
      updatedTodos[editingIndex] = editText;
      setTodos(updatedTodos);
      setEditingIndex(-1);
      setEditText("");
    }
  };

  const deleteTodo = (index) => {
    const filteredTodos = todos.filter((_, todoIndex) => todoIndex !== index);
    setTodos(filteredTodos);
  };

  return (
    <div>
      <Nav />
      <div className="container">
        <div className="row">
          <form action="#" className="form mt-5">
            <input
              value={editingIndex === -1 ? name : editText}
              onChange={(e) =>
                editingIndex === -1
                  ? setName(e.target.value)
                  : setEditText(e.target.value)
              }
              type="text"
              className="form-control my-3"
            />
            <button
              className="btn btn-info my-3 mx-3"
              onClick={() => setName("")}
            >
              Clear
            </button>
            {editingIndex === -1 ? (
              <button className="btn btn-success " onClick={addTodo}>
                Add
              </button>
            ) : (
              <button className="btn btn-success " onClick={updateTodo}>
                Update
              </button>
            )}
          </form>
        </div>
        <div>
          {/* <> list-inline</> */}
          <ol className="d-flex flex-column flex-wrap">
            {todos.map((todo, index) => (
              <li key={index} className="my-3  px-2 ">
                {todo}{" "}
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => editTodo(index)}
                >
                  Edit
                </button>{" "}
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => deleteTodo(index)}
                >
                  Delete
                </button>
              </li>
            ))}
          </ol>
        </div>
      </div>
    </div>
  );
}
